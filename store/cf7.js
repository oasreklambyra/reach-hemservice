export const state = () => ({
  cf: { },
  wordpress: 'https://wordpress.reachhemservice.se/wp-json/'

})

export const mutations = {
  getContactform (state, data) {
    const temporaryHolder = state.cf
    state.cf = [] // Updates the structure
    temporaryHolder[data.id] = data.data
    state.cf = temporaryHolder
  }
}

export const actions = {

  async getContactform (context, payload) {
    const res = await this.$axios.get(context.state.wordpress + 'core/contactform?id=' + payload.id)

    context.commit('getContactform', {
      id: payload.id,
      data: res.data
    })
  }
}
