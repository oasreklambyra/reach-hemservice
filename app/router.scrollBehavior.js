export default async function(to, from, savedPosition) {

	const findEl = async (hash, x = 0) => {

	return (
	  document.getElementById(hash) ||
	  new Promise(resolve => {
	    if (x > 50) {
	      return resolve(document.getElementById("__nuxt"));
	    }
	    setTimeout(() => {
	      resolve(findEl(hash, ++x || 1));
	    }, 100);
	  })
	);
	};

	if (to.hash && to.hash !== '#readmore' && to.hash !== '#readless') {
	
		let el = await findEl(to.hash.replace('#', ''));
	
		if ("scrollBehavior" in document.documentElement.style) {
		  return window.scrollTo({ top: el.offsetTop - 100, behavior: "smooth" });
		} else {
		  return window.scrollTo(0, el.offsetTop - 100);
		}
	}

	if(to.path === from.path) {
		// Page didn't change
		return false
	}

	if (savedPosition) {
		return savedPosition;
	}

	return { x: 0, y: 0 };
}
