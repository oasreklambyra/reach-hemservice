const middleware = {}

middleware['getMeta'] = require('../middleware/getMeta.js')
middleware['getMeta'] = middleware['getMeta'].default || middleware['getMeta']

middleware['getPage'] = require('../middleware/getPage.js')
middleware['getPage'] = middleware['getPage'].default || middleware['getPage']

middleware['getSupportData'] = require('../middleware/getSupportData.js')
middleware['getSupportData'] = middleware['getSupportData'].default || middleware['getSupportData']

export default middleware
