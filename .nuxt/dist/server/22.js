exports.ids = [22];
exports.modules = {

/***/ 112:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./components/blocks/PagebuilderBaseService.vue?vue&type=template&id=992e472c&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"base_service-wrapper"},[_vm._ssrNode("<div class=\"container\">","</div>",[_c('BaseBlockTop',{attrs:{"title":_vm.data.title,"url":_vm.data.link.url,"linkTitle":_vm.data.link.title}}),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"row justify-content-center gutter\">","</div>",_vm._l((_vm.data.service),function(service,i){return _vm._ssrNode("<div class=\"col-md-4 col-sm-6 service-item\">","</div>",[_vm._ssrNode("<div class=\"service-item-inner box\">","</div>",[_c('nuxt-link',{attrs:{"to":service.permalink}},[_c('img',{attrs:{"src":service.featured_image.large,"alt":service.post_name}}),_vm._v(" "),_c('div',{staticClass:"meta"},[_c('h4',[_vm._v(_vm._s(service.post_title))]),_vm._v(" "),(service.post_excerpt)?_c('p',[_vm._v("\n                "+_vm._s(service.post_excerpt.slice(0, 100) + '...')+"\n              ")]):_vm._e()])])],1)])}),0)],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/blocks/PagebuilderBaseService.vue?vue&type=template&id=992e472c&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/vue-loader/lib??vue-loader-options!./components/blocks/PagebuilderBaseService.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var PagebuilderBaseServicevue_type_script_lang_js_ = ({
  components: {
    BaseBlockTop: () => __webpack_require__.e(/* import() */ 1).then(__webpack_require__.bind(null, 117))
  },
  props: {
    data: {
      type: [Object, Array]
    }
  }
});
// CONCATENATED MODULE: ./components/blocks/PagebuilderBaseService.vue?vue&type=script&lang=js&
 /* harmony default export */ var blocks_PagebuilderBaseServicevue_type_script_lang_js_ = (PagebuilderBaseServicevue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(3);

// CONCATENATED MODULE: ./components/blocks/PagebuilderBaseService.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(89)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  blocks_PagebuilderBaseServicevue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "1dcbc491"
  
)

/* harmony default export */ var PagebuilderBaseService = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(90);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(5).default
module.exports.__inject__ = function (context) {
  add("043ff454", content, true, context)
};

/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseService_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(58);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseService_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseService_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseService_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_vue_loader_lib_index_js_vue_loader_options_PagebuilderBaseService_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 90:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(4);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".base_service-wrapper .container .row.gutter{margin:0 -2rem}.base_service-wrapper .container .row.gutter>*{padding:0 2rem}.base_service-wrapper .container .row .service-item{margin-bottom:4rem}.base_service-wrapper .container .row .service-item .service-item-inner{transition:all .25s ease}.base_service-wrapper .container .row .service-item .service-item-inner:hover{background:#fff;box-shadow:12px 12px 25px rgba(16,24,32,.15);transform:translateY(-.2rem)}.base_service-wrapper .container .row .service-item .service-item-inner a{text-decoration:none;color:#101820}.base_service-wrapper .container .row .service-item .service-item-inner img{width:100%;height:100%;border-radius:.8rem;box-shadow:10px 10px 30px rgba(16,24,32,.1);margin-bottom:2rem}.base_service-wrapper .container .row .service-item .service-item-inner .meta h4{font-weight:700;margin-bottom:.5rem}@media (max-width:1280px){.base_service-wrapper .container .row .service-item .service-item-inner .buttons-wrapper a{padding:1rem}}@media (max-width:1280px){.base_service-wrapper .container .row .service-item .service-item-inner .buttons-wrapper a svg{margin:0}}@media (max-width:1280px){.base_service-wrapper .container .row .service-item .service-item-inner .buttons-wrapper a span{display:none}}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ })

};;
//# sourceMappingURL=22.js.map