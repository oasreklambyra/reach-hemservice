import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _41071640 = () => interopDefault(import('../pages/nyheter/index.vue' /* webpackChunkName: "pages/nyheter/index" */))
const _2bb94300 = () => interopDefault(import('../pages/tjanster/index.vue' /* webpackChunkName: "pages/tjanster/index" */))
const _b8a5bf26 = () => interopDefault(import('../pages/nyheter/_post.vue' /* webpackChunkName: "pages/nyheter/_post" */))
const _31c3e22a = () => interopDefault(import('../pages/tjanster/_services.vue' /* webpackChunkName: "pages/tjanster/_services" */))
const _78c199b6 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _36204156 = () => interopDefault(import('../pages/_page/index.vue' /* webpackChunkName: "pages/_page/index" */))
const _3eb07b80 = () => interopDefault(import('../pages/_page/_subPage/index.vue' /* webpackChunkName: "pages/_page/_subPage/index" */))
const _647aef1c = () => interopDefault(import('../pages/_page/_subPage/_subSubPage.vue' /* webpackChunkName: "pages/_page/_subPage/_subSubPage" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/nyheter",
    component: _41071640,
    name: "nyheter"
  }, {
    path: "/tjanster",
    component: _2bb94300,
    name: "tjanster"
  }, {
    path: "/nyheter/:post",
    component: _b8a5bf26,
    name: "nyheter-post"
  }, {
    path: "/tjanster/:services",
    component: _31c3e22a,
    name: "tjanster-services"
  }, {
    path: "/",
    component: _78c199b6,
    name: "index"
  }, {
    path: "/:page",
    component: _36204156,
    name: "page"
  }, {
    path: "/:page/:subPage",
    component: _3eb07b80,
    name: "page-subPage"
  }, {
    path: "/:page/:subPage/:subSubPage",
    component: _647aef1c,
    name: "page-subPage-subSubPage"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
