import Vue from 'vue';

import {
  BButton,
  BFormInput,
  BFormCheckboxGroup,
  BModal,
  BBadge,
  BTabs,
  BTab,
  BFormInvalidFeedback,
  BCarousel,
  BCarouselSlide,
  VBTooltip
} from 'bootstrap-vue';

Vue.component('BButton', BButton);
Vue.component('BFormInput', BFormInput);
Vue.component('BFormCheckboxGroup', BFormCheckboxGroup);
Vue.component('BModal', BModal);
Vue.component('BBadge', BBadge);
Vue.component('BTabs', BTabs);
Vue.component('BTab', BTab);
Vue.component('BFormInvalidFeedback', BFormInvalidFeedback);
Vue.component('BCarousel', BCarousel);
Vue.component('BCarouselSlide', BCarouselSlide);

Vue.directive('BTooltip', VBTooltip);
