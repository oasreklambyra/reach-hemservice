export default async function (context) {
  const { store, redirect, route } = context

  if (!route.path.endsWith('/')) {
    let query = ''

    if (Object.keys(route.query).length > 0) {
      query = '?'
      query += Object.keys(route.query)
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(route.query[key])}`)
        .join('&')
    }

    return redirect(route.path + '/' + query)
  }

  if (!store.state.meta.meta[route.path]) {
    await store.dispatch({ type: 'meta/getMeta', path: route.path })
  }

  console.log(route.path)
  const meta = store.state.meta.meta[route.path]

  /* Format to json */
  const res = {
    meta: [],
    link: [],
    script: []
  }

  if (meta && meta.html) {
    let newhtml = meta.html
    newhtml = newhtml.replace(/:\/\/wordpress./g, '://www.')

    const metaArr = newhtml.split('\n')
    // console.log(metaArr)
    // test
    metaArr.forEach((tag) => {
      if ((tag.includes('<meta') || tag.includes('<link')) && tag.includes('/>')) {
        let type = false

        if (tag.includes('meta')) {
          type = 'meta'
          tag = tag.replace('meta', '')
        }

        if (tag.includes('link')) {
          type = 'link'
          tag = tag.replace('link', '')
        }

        tag = tag.replace(/</g, '{')
        tag = tag.replace(/\/>/g, '}')
        tag = tag.replace(/=/g, '":')
        tag = tag.replace(/" /g, '","')
        tag = tag.replace(/","}/g, '"}')
        tag = tag.replace(/{ /g, '{"')

        res[type].push(JSON.parse(tag))
      }
    })

    /* Add hid */
    const resFormated = { ...context.app.html }

    resFormated.link = [
      { rel: 'icon', type: 'image/x-icon', href: '/static/favicon.jpg' },
      { rel: 'stylesheet', href: 'https://use.typekit.net/zve0qfl.css' }
    ]

    resFormated.meta = [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ]

    res.meta.forEach((tag) => {
      tag.hid = tag.name || tag.property
      resFormated.meta.push(tag)

      if (tag.hid === 'og:title') {
        resFormated.title = tag.content.replace('amp;', '').replace('amp;', '')
      }
    })

    res.link.forEach((tag) => {
      tag.hid = tag.rel
      resFormated.link.push(tag)
    })

    await store.dispatch({ type: 'meta/setMetaFormated', path: route.path, data: resFormated })

    context.app.html = resFormated
  }
}
