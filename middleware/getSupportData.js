/*
  Get arcive data of a CPT
 */

export default async function (context) {
  const { route } = context
  const data = context.app.router.app.getPageLogic(route)

  // Fetch posts on every every page
  if (!context.app.store.state.options) {
    await context.app.store.dispatch({ type: 'getOptions' })
  }

  if (!context.app.store.state.sitemap) {
    await context.app.store.dispatch({ type: 'getSitemap' })
  }

  // if (route.name === 'personal') {
  //   if (!context.app.store.state.cpt.employee) {
  //     await context.app.store.dispatch({ type: 'getCpt', postType: 'employee' })
  //   }
  // }

  if (route.name === 'tjanster') {
    if (!context.app.store.state.cpt.services) {
      await context.app.store.dispatch({ type: 'getCpt', postType: 'services' })
      // console.log(context.app.store.state.cpt.services)
    }
  }

  // Contact Form 7 Booking
  await context.app.store.dispatch({ type: 'cf7/getContactform', id: 21 })

  /* Pagebuilder */

  const page = context.app.store.state.pages[data.type + '/' + data.name]

  if (page && page.custom_fields && page.custom_fields.pagebuilder) {
    for (let i = 0; i < page.custom_fields.pagebuilder.length; i++) {
      const block = page.custom_fields.pagebuilder[i]

      if (block.acf_fc_layout === 'base_contact') {
        if (!context.app.store.state.cf7.cf[block.form.form_id]) {
          await context.app.store.dispatch({ type: 'cf7/getContactform', id: block.form.form_id })
        }
      }
    }
  }
}
