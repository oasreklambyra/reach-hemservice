import Vue from 'vue'
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload, {
  preLoad: 1.3,
  // loading: '/placeholder.svg',

  error: '/placeholder.svg',
  attempt: 1
})
